  **# Currency Rates App: Empowering Informed Financial Decisions**

**## Streamline Your Currency Insights with This User-Friendly Python Toolkit**

Stay ahead of the curve with real-time currency exchange rates at your fingertips! This meticulously crafted Python application offers a seamless gateway to current rates directly from tgju.org: [https://www.tgju.org/currency](https://www.tgju.org/currency), designed to enhance your financial decision-making with key features:

**## Key Features**

- **Comprehensive Currency Information:** Explore a detailed list of currencies, including current rates, last updates, minimum rates, and maximum rates.
- **Effortless Data Preservation:** Save valuable currency data in Excel format for further analysis, record-keeping, and strategic financial planning.
- **Seamless Exploration of Details:** Dive deeper into specific currencies with a simple double-click, opening their main website pages directly within the application.
- **Always Up-to-Date:** Ensure you're working with the latest information as the application automatically refreshes displayed rates every 10 minutes.

**## Installation**

### 1. Gather the Required Libraries:

```bash
pip install requests beautifulsoup4 tk ttk pandas
```

### 2. Activate the Application:

```bash
python currency_app.py
```

**## Usage**

### 1. Launch and Explore: Upon opening the app, you'll be greeted with an intuitive interface showcasing a comprehensive list of currencies and their respective rates.

### 2. Effortlessly Save Data: Preserve the displayed currency data to an Excel file for future reference by clicking the conveniently placed "Save to Excel" button.

### 3. Uncover Additional Details: Delve deeper into specific currencies with a simple double-click on their entries, which immediately opens their main website pages.

### 4. Stay Informed: The application meticulously refreshes the displayed rates every 10 minutes, ensuring you're always working with the most current information.

**## Dependencies**

- Python 3.x
- requests
- beautifulsoup4
- tk
- ttk
- pandas

**## Author**

[seyed Nematolla Kianejad]


**## Contributing**

We value collaboration! Feel free to submit pull requests or open issues to address bugs or propose enhancements. Together, we can elevate this tool to its full potential!


**##Using .exe file**
In dist folder just download the file and oepn it the app will start
